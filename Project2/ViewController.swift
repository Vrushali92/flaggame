//
//  ViewController.swift
//  Project2
//
//  Created by Vrushali Kulkarni on 04/05/20.
//  Copyright © 2020 Vrushali Kulkarni. All rights reserved.
//

import UIKit

final class ViewController: UIViewController {

    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    @IBOutlet weak var button3: UIButton!
    
    @IBAction func buttonTapped(_ sender: UIButton) {

        var title: String
        var message: String

        if sender.tag == correctAnswer {
            title = "Correct"
            message = "You have selected correct flag!"
            score += 1
        } else {
            title = "Wrong!"
            message = "That's the flag of \(countries[sender.tag])"
            score -= 1
        }

        if type(of: self).askedQuestionCounter == 10 {
            title = "Message"
            message = "Your score is \(score)"
        }

        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)

        let alertAction = UIAlertAction(title: "Continue",
                                        style: .default,
                                        handler: askQuestion)
        alert.addAction(alertAction)
        present(alert, animated: true, completion: nil)
    }


    var countries = [String]()
    var score = 0
    var correctAnswer = 0
    static var askedQuestionCounter = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        countries += ["estonia", "france", "germany", "ireland",
                      "italy", "nigeria", "poland", "monaco",
                      "russia", "spain", "uk", "us"]

        configureUI()
    }

    func configureUI() {

        button1.layer.borderColor = UIColor(red: 1.0, green: 0.6, blue: 0.2, alpha: 1.0).cgColor
        button2.layer.borderColor = UIColor(red: 1.0, green: 0.6, blue: 0.2, alpha: 1.0).cgColor
        button3.layer.borderColor = UIColor(red: 1.0, green: 0.6, blue: 0.2, alpha: 1.0).cgColor

        button1.layer.borderWidth = 1
        button2.layer.borderWidth = 1
        button3.layer.borderWidth = 1

        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Score",
                                                            style: .done,
                                                            target: self,
                                                            action: #selector(showScore))

        askQuestion()
    }

    func askQuestion(action: UIAlertAction! = nil) {

        type(of: self).askedQuestionCounter += 1
        countries.shuffle()

        button1.setImage(UIImage(named: countries[0]), for: .normal)
        button2.setImage(UIImage(named: countries[1]), for: .normal)
        button3.setImage(UIImage(named: countries[2]), for: .normal)

        correctAnswer = Int.random(in: 0...2)

        title = countries[correctAnswer].uppercased()
    }

    @objc func showScore() {

        let alert = UIAlertController(title: "Score",
                                      message: "\(score)",
                                      preferredStyle: .alert)

        let alertAction = UIAlertAction(title: "Continue",
                                        style: .default,
                                        handler: nil)
        alert.addAction(alertAction)
        present(alert, animated: true, completion: nil)
    }
}

